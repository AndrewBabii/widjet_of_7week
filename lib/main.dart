import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double _counter = 0;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: 'Flutter Demo ',
                  style: TextStyle(fontSize: 26),
                ),
                TextSpan(
                  text: ' Home Page',
                  style: TextStyle(fontSize: 18),
                ),
              ],
            ),
          ),
          bottom: TabBar(
            tabs: [
              Tab(
                text: 'first',
              ),
              Tab(
                text: 'slider',
              ),
              Tab(
                text: 'magic',
              ),
            ],
          ),
        ),
        body: Builder(
          builder: (BuildContext context) {
            return TabBarView(
              children: [
                Image.network('https://i.pinimg.com/originals/a5/df/c2/a5dfc21fc9ad530068537135aef51ba5.png'),
                Slider.adaptive(
                  value: _counter,
                  onChanged: (newCounter) {
                    setState(() => _counter = newCounter);
                  },
                ),
                Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1tRe0NG4EgSupPmrCUkSXwxEIOLKH9vJkoA&usqp=CAU'),
              ],
            );
          },
        ),
      ),
    );
  }
}
